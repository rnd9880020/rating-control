Pod::Spec.new do |s|
  s.name         = "RatingControl"
  s.version      = "1.0.0"
  s.summary      = "Custom UI for giving food rating."
  s.homepage     = "https://gitlab.com/rnd9880020/rating-control"
  s.license    = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "Faruuq" => "faruuqq@icloud.com" }
  s.platform     = :ios, "11.0"
  s.source       = { :git => "https://gitlab.com/rnd9880020/rating-control.git", :tag => "1.0.0" }
  # s.source_files  = "Classes", "Classes/**/*.{h,m}"
  # s.exclude_files = "Classes/Exclude"
  # s.public_header_files = "Classes/**/*.h"
  # s.framework  = "RatingControl"
  s.vendored_frameworks = "builds/RatingControl.xcframework"
  # s.vendored_frameworks = "RatingControl"
  # s.public_header_files = "RatingControl.framework/Headers/*.h"
end
